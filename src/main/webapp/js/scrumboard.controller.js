(function(){
    'use strict';

    angular
        .module('scrumboard.demo')
        .controller('ScrumboardController', ScrumboardController);

    ScrumboardController.$inject = ['$http', '$location', '$routeParams'];

    function ScrumboardController($http, $location, $routeParams) {
        var vm = this;

        vm.add = add;

        activate();

        function activate() {
            vm.data = [];
            var url = '/projects/' + $routeParams.projectId;
            $http.get(url).then(
                function (response) {
                    vm.project = response.data;
                }, function(){
                    alert('Could not load project :(');
                }
            );
        }


        function add(list, title) {
            var card = {
        		id: null,
                list: "/lists/" + list.id,
                title: title
            };

            $http.post('/cards/', card)
                .then(function (response) {
                    list.cards.push(response.data);
                },
                function(){
                    alert('Could not create card');
                }
            );
        };

    }

})();