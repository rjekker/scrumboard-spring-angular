(function(){
    angular.module('scrumboard.demo')
        .controller('ProjectsController', ProjectsController);

    ProjectsController.$inject = ['$http', '$location'];

    function ProjectsController ($http, $location) {
        var vm = this;

        activate();

        function activate(){
            $http.get('/projects/')
                .then(function(response){
                    vm.data = response.data._embedded.projects;
                });
        }
    }
})();
