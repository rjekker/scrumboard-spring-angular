(function () {
    'use strict';

    angular.module('scrumboard.demo')
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {

        $routeProvider
            .when('/project/:projectId', {
                templateUrl: '/html/scrumboard.html',
                controller: 'ScrumboardController',
                controllerAs: 'vm'
            })
            .when('/', {
                templateUrl: '/html/projects.html',
                controller: 'ProjectsController',
                controllerAs: 'vm'
            })
            .otherwise('/');
    }
})();

