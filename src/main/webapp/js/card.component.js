(function () {
    'use strict';

    angular.module('scrumboard.demo')
        .component('scrumboardCard', {
            templateUrl: '/html/card.html',
            controller: CardComponentController,
            bindings: {'card': '=', list: '=', project: '='},
            controllerAs: 'vm'
        });

    CardComponentController.$inject = ['$http'];

    function CardComponentController ($http) {
        var vm = this;

        vm.update = update;
        vm.move = move;
        vm.remove = remove;
        vm.modelOptions = {
            debounce: 500
        };

        activate();
        function activate(){
            vm.url = '/cards/' + vm.card.id + '/';
            vm.destList = vm.list;
        }


        function update () {
            return $http.put(
                vm.url,
                vm.card
            );
        };


        function removeCardFromList(card, list){
            var cards = list.cards;
            cards.splice(
                cards.indexOf(card),
                1
            );
        }


        function move () {
            if(vm.destList === undefined){
                return;
            }
            vm.card.list = "/cards/" + vm.destList.id;
            update().then(function() {
                 {
                    removeCardFromList(vm.card, vm.list);
                    vm.destList.cards.push(vm.card);
                }
            });
        }


        function remove (){
            $http.delete(vm.url).then(
                function(){
                    removeCardFromList(vm.card, vm.list);
                }
            );
        };
    }
})();
