package nl.rjekker;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class LoginViewConfig extends WebMvcConfigurerAdapter {

	/* Hier configureren we de view-naam voor de login controller
	 * 
	 * De login controller wordt door spring security aangeleverd
	 * Maar wij leveren de jsp
	 * En hier geven we aan hoe die heet...
	 */
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/login").setViewName("login");
	}

}
