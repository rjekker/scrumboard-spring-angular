package nl.rjekker;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

import nl.rjekker.model.Card;
import nl.rjekker.model.List;
import nl.rjekker.model.Project;

@Configuration
public class CrudRepositoryConfig extends RepositoryRestMvcConfiguration {

	@Override
	protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(Card.class, List.class, Project.class);
	}
}
