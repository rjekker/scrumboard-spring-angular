package nl.rjekker.model;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "inlineCards", types = { List.class })
public interface InlineCardsProjection {
	Long getId();
	String getName();
	java.util.List<Card> getCards();
}
