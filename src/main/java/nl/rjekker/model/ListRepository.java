package nl.rjekker.model;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(excerptProjection = InlineCardsProjection.class)
public interface ListRepository extends CrudRepository<List, Long> {

}
