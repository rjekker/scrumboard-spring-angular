package nl.rjekker.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class List {
	private Long id;
	private String name;
	private Project project;
	private java.util.List<Card> cards;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@ManyToOne
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	@OneToMany(mappedBy="list")
	public java.util.List<Card> getCards() {
		return cards;
	}
	public void setCards(java.util.List<Card> cards) {
		this.cards = cards;
	}
	
	
}
