package nl.rjekker.model;

import org.springframework.data.rest.core.config.Projection;

@Projection(name = "inlineLists", types = { Project.class })
public interface InlineListsProjection {

	java.util.List<List> getLists();
}
