package nl.rjekker.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Project {
	private Long id;
	private String name;
	private java.util.List<List> lists;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@OneToMany(mappedBy="project")
	public java.util.List<List> getLists() {
		return lists;
	}
	public void setLists(java.util.List<List> lists) {
		this.lists = lists;
	}
	
	
}
